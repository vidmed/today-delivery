<?php

namespace box2box\today_delivery;

require dirname(__FILE__).'/TodayDeliverySign.php';

use Exception;
use Guzzle\Http\Client;

class TodayDeliverySdk
{
    /**
     * @var - Версия API
     */
    private $version;

    /**
     * @var - url API
     */
    private $baseUrl;

    private $login;

    private $password;

    /**
     * @var - Уникальный идентификатор пользователя
     */
    private $memberId;

    /**
     * @var - Хэш-код для подписи запросов от пользователя.
     */
    private $privateKey;

    /**
     * @var - экземпляр класса-генратора подписи запросов
     */
    private $signInstance;

    /**
     * @var string - ошибки возвращаемые сервером
     */
    private $errors = '';

    /**
     * @param $url - урл, на который будем стучать
     * Например, https://api-test.todaydelivery.ru
     * @param $login
     * @param $password
     * @param $version - версия API
     * @param bool $decode_json - конвертировать ли json-строку ответа
     * @throws Exception
     */
    public function __construct($base_url, $version = 1, $login=null, $password=null)
    {
        if (
            is_string($base_url) && !empty($base_url)
            && !empty($version)
        ) {
            $this->baseUrl = trim($base_url, " \t\n\r\0\x0B/");
            $this->version = trim($version);
        } else {
            throw new Exception('Base_url and version might be a non empty string!');
        }
        if(!empty($login) && !empty($password)){
            $this->login = $login;
            $this->password = $password;
        }
    }

    /**
     * Аутентификация и авторизация Пользователя
     * @param string $login - Телефонный номер в любом формате ( будут оставлены только цифры ). Позже: email, login
     * @param string $password - Пароль передаётся в открытом виде, так как передача происходит по HTTPS
     * @throws Exception
     */
    public function login($login=null, $password=null)
    {
        $login = $login ? : $this->login;
        $password = $password ? : $this->password;
        if (empty($login) || empty($password)) {
            throw new Exception('Login and password might be non empty!');
        }
        $url = $this->baseUrl . '/client/v' . $this->version . '/login';
        $login_params = [
            'login'=>$login,
            'password'=>$password
        ];

        $response = $this->call($url, $login_params);

        if($response->result->status == 'ok'){
            $this->memberId = $response->result->memberId;
            $this->privateKey = $response->result->privateKey;
        }

        return $response;
    }

    /**
     * Регистрация пользователя
     * @param string $login - Телефонный номер в любом формате ( будут оставлены только цифры ). Позже: email, login
     * @param string $password - Пароль передаётся в открытом виде, так как передача происходит по HTTPS
     * @param mixed $has_app - Идентификатор регистрации с приложения
     * @param mixed $push_notification - Идентификатор возможности отсылать пользователю пуш уведомления
     * @param string $email - Адрес электронной почты
     * @param string $first_name - Имя. Как вариант, в этом поле может содержаться ФИО полностью, но это не желательно
     * @param string $last_name - Фамилия
     * @param string $middle_name - Отчество
     * @param bool $client - Как регистрируется пользователь, как курьер или как клиент.
     * Если клиент(отправитель или получатель), то указывается true. Если как курьер то можно ничего не указывать.
     */
    public function registration($login=null, $password=null, $has_app=null, $push_notification=null, $email=null, $first_name=null, $last_name=null, $middle_name=null, $client=true)
    {
        $login = $login ? : $this->login;
        $password = $password ? : $this->password;
        if (empty($login) || empty($password)) {
            throw new Exception('Login and password might be non empty!');
        }
        $registration_params = [
            'phonePrimary' => $login,
            'password' => $password,
        ];
        if(isset($has_app)) $registration_params['hasApp'] = (int)$has_app;
        if(isset($push_notification)) $registration_params['pushNotification'] = (int)$push_notification;
        if(!empty($email)) $registration_params['email'] = (string)$email;
        if(!empty($first_name)) $registration_params['firstName'] = (string)$first_name;
        if(!empty($last_name)) $registration_params['lastName'] = (string)$last_name;
        if(!empty($middle_name)) $registration_params['middleName'] = (string)$middle_name;
        if(isset($client)) $registration_params['client'] = (bool)$client ? 'true' : 'false';

        $url = $this->baseUrl . '/client/v' . $this->version . '/registration';

        return $this->call($url, $registration_params);
    }

    /**
     * Метод получения данных по заказу
     * @param int $order_id - Идентификатор Заказа
     * @return object
     * @throws Exception
     */
    public function selectOrder($order_id)
    {
        $params['orderId'] = $order_id;
        $url = $this->baseUrl . '/client/v' . $this->version . '/selectOrder';
        return $this->signedCall($url);
    }

    /**
     * Создание заказа
     * @param string $city_from - Город отправления. Может отсутствовать, если есть addressIdFrom
     * @param string $address_from - Адрес отправления. Может отсутствовать, если есть addressIdFrom
     * @param string $city_to - Город назначения. Может отсутствовать, если есть addressIdTo
     * @param string $address_to - Адрес назначения. Может отсутствовать, если есть addressIdTo
     * @param int $address_id_from - Идентификатор адреса отправления
     * @param int $address_id_to - Идентификатор адреса получения
     * @param array $parcel_items - будет преобразован в JSON с массивом объектов, описывающих содержимое Посылки. Не показывается Курьеру.
     * @param int $payment_method - Способ оплаты: 1: оплачен; 2: наличные, не оплачен; 3: наличные, оплачено только содержимое; 4: наличные, оплачена доставка; 5: банковская карта на месте, не оплачен; 6: банковская карта на месте, оплачено только содержимое; 7: банковская карта на месте, оплачена только доставка; 8: безнал;
     * @param int $weight - Вес посылки в граммах
     * @param int $length - Длина Посылки в миллиметрах
     * @param int $width - Ширина Посылки в миллиметрах
     * @param int $height - Высота Посылки в миллиметрах
     * @param int $delivery_type - Тип доставки: 1 - до двери; 2 - до ПВЗ
     * @param float $parcel_cost - Стоимость отправляемой Посылки. То, сколько Курьер должен взять с Получателя. Может равняться нулю.
     * @param string $dt_pickup_from - Дата и время начала интервала, когда нужно забрать Посылку
     * @param string $dt_pickup_to - Дата и время окончания интервала, когда нужно забрать Посылку
     * @param string $dt_destination_from - Дата и время начала интервала, когда нужно вручить Посылку
     * @param string $dt_destination_to - Дата и время окончания интервала, когда нужно вручить Посылку
     * @param int $airport - Забор или вручение Посылки происходит в аэропорту: 0 - нет, 1 - да.
     * @param string $cmt - Комментарий к заказу
     * @param string $code_promo - Код на скидку
     * @param int $receiver_member_id - Идентификатор пользователя-контрагента. Должен отсутствовать, если указаны receiverUserPhone и receiverUserName.
     * @param string(50) $receiver_user_phone - Телефонный номер контрагента. Должен отсутствовать, если указан receiverMemberId.
     * @param string(50) $receiver_user_name - Имя контрагента. Должен отсутствовать, если указан receiverMemberId.
     * @param int $receiver_organization_id - Идентификатор компании-контрагента. Может отсутствовать, если указано receiverOrganizationName.
     * @param string(255) $receiver_organization_name - Название компании-контрагента. Может отсутствовать, если указано receiverOrganizationId.
     * @param int $sender_member_id - Уникальный идентификатор отправителя, если не указано берется из memberId
     * @param int $sender_organization_id - Уникальный идентификатор организации отправителя( Если не указан, то доставка осуществляется не от имени организации)
     * @param string(255) $external_num_order - Номер заказа во внешней системе(интернет магазин и тд)
     * @return object
     * @throws Exception
     */
    public function createOrder($city_from, $address_from, $city_to, $address_to, $address_id_from, $address_id_to, array $parcel_items, $payment_method, $weight, $length=null, $width=null, $height=null, $delivery_type=null, $parcel_cost=null, $dt_pickup_from=null, $dt_pickup_to, $dt_destination_from=null, $dt_destination_to, $airport=null, $cmt=null, $code_promo=null, $receiver_member_id, $receiver_user_phone, $receiver_user_name, $receiver_organization_id=null, $receiver_organization_name=null, $sender_member_id=null, $sender_organization_id=null, $external_num_order=null)
    {
        $params = [];
        if(isset($city_from)) $params['cityFrom'] = (string)$city_from;
        if(isset($address_from)) $params['addressFrom'] = (string)$address_from;
        if(isset($city_to)) $params['cityTo'] = (string)$city_to;
        if(isset($address_to)) $params['addressTo'] = (string)$address_to;
        if(isset($address_id_from)) $params['addressIdFrom'] = (int)$address_id_from;
        if(isset($address_id_to)) $params['addressIdTo'] = (int)$address_id_to;
        if(isset($parcel_items)) $params['parcelItems'] = json_encode($parcel_items, JSON_UNESCAPED_UNICODE);
        if(isset($payment_method)) $params['paymentMethod'] = (int)$payment_method;
        if(isset($weight)) $params['weight'] = (int)$weight;
        if(isset($length)) $params['length'] = (int)$length;
        if(isset($width)) $params['width'] = (int)$width;
        if(isset($height)) $params['height'] = (int)$height;
        if(isset($delivery_type)) $params['deliveryType'] = (int)$delivery_type;
        if(isset($parcel_cost)) $params['parcelCost'] = (float)$parcel_cost;
        if (!empty($dt_pickup_from) && strtotime($dt_pickup_from) === false) {
            throw new Exception('$dt_pickup_from variables migth be a valid string for transfer to date.');
        } else {
            $params['dtPickupFrom'] = date('Y-m-d H:i:s', strtotime($dt_pickup_from));
        }
        if (!empty($dt_pickup_to) && strtotime($dt_pickup_to) === false) {
            throw new Exception('$dt_pickup_to variables migth be a valid string for transfer to date.');
        } else {
            $params['dtPickupTo'] = date('Y-m-d H:i:s', strtotime($dt_pickup_to));
        }
        if (!empty($dt_destination_from) && strtotime($dt_destination_from) === false) {
            throw new Exception('$dt_destination_from variables migth be a valid string for transfer to date.');
        } else {
            $params['dtDestinationFrom'] = date('Y-m-d H:i:s', strtotime($dt_destination_from));
        }
        if (!empty($dt_destination_to) && strtotime($dt_destination_to) === false) {
            throw new Exception('$dt_destination_to variables migth be a valid string for transfer to date.');
        } else {
            $params['dtDestinationTo'] = date('Y-m-d H:i:s', strtotime($dt_destination_to));
        }
        if(isset($airport)) $params['airport'] = (int)$airport;
        if(isset($cmt)) $params['cmt'] = (string)$cmt;
        if(isset($code_promo)) $params['codePromo'] = (string)$code_promo;
        if(isset($receiver_member_id)) $params['receiverMemberId'] = (int)$receiver_member_id;
        if(isset($receiver_user_phone)) $params['receiverUserPhone'] = (string)$receiver_user_phone;
        if(isset($receiver_user_name)) $params['receiverUserName'] = (string)$receiver_user_name;
        if(isset($receiver_organization_id)) $params['receiverOrganizationId'] = (int)$receiver_organization_id;
        if(isset($receiver_organization_name)) $params['receiverOrganizationName'] = (string)$receiver_organization_name;
        if(isset($sender_member_id)) $params['senderMemberId'] = (int)$sender_member_id;
        if(isset($sender_organization_id)) $params['senderOrganizationId'] = (int)$sender_organization_id;
        if(isset($external_num_order)) $params['externalNumOrder'] = (string)$external_num_order;

        $url = $this->baseUrl . '/sender/v' . $this->version . '/createOrder';

        return $this->signedCall($url, $params);
    }

    /**
     * Метод получения идентификаторов активных заказов
     * @return object
     * @throws Exception
     */
    public function selectActiveOrders()
    {
        $url = $this->baseUrl . '/sender/v' . $this->version . '/selectActiveOrders';
        return $this->signedCall($url);
    }

    /**
     * Метод получения истории выполненных заказов отправителя
     * @return object
     * @throws Exception
     */
    public function selectHistoryOrders()
    {
        $url = $this->baseUrl . '/sender/v' . $this->version . '/selectHistoryOrders';
        return $this->signedCall($url);
    }

    private function signedCall($url, $params=null)
    {
        if(empty($this->memberId) || empty($this->privateKey)){
            throw new Exception('memberId and privateKey might not be empty. Use login method.');
        }

        $params['memberId'] = $this->memberId;
        $params['dt'] = date('Y-m-d H:i:s', strtotime('now'));
        $params['sign'] = $this->getSignInstance()->generateSignKey($params, $this->privateKey);

        return $this->call($url, $body=null);
    }

    private function getSignInstance()
    {
        if(empty($this->signInstance)){
            $this->signInstance = new TodayDeliverySign();
        }
        return $this->signInstance;
    }

    /** Общий метод для отправки POST запроса
     * @param $url - на какой url отправлять запрос
     * @param $decode_json - отдавать json-строку или конвертировать в объект
     * @param mixed $body - тело запроса (для метода POST)
     * @return object
     * @throws Exception
     */
    private function call($url, $body=null)
    {
        try{
            $client = new Client();
            $request = $client->post($url, null, $body);
            $request->setPort(443);
            $response = $request->send();
            if($response->isSuccessful()){
                if(!$response->getBody()->getContentLength()){
                    return (object)['result'=> null,  'url'=>$request->getUrl()];
                }

                return (object)['result'=>json_decode($response->getBody(true)), 'url'=>$request->getUrl()];
            }

            if($this->processError($response)){
                throw new Exception($this->errors);
            }

            throw new Exception($response->getMessage());
        }
        catch(Exception $e){
            if(method_exists($e, 'getResponse')){
                //нормальное описание ошибки приходи в теле ответа
                $error_respoce = $e->getResponse();
                if($this->processError($error_respoce)){
                    throw new Exception($this->errors, 0, $e);
                }
            }

            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function processError($response)
    {
        //нормальное описание ошибки приходи в теле ответа
        $error_body = json_decode($response->getBody(true),true);
        //проверка на валидный json в теле ответа
        if(json_last_error() == JSON_ERROR_NONE && $error_body['status']=='error'){
            if(is_array($error_body['errors'])){
                foreach($error_body['errors'] as $key=>$error){
                    $error_arr[] = $key.': '.$error;
                }
                $this->errors = join(', ', $error_arr);
            } else {
                $this->errors = $error_body['errors'];
            }

            return true;
        }

        return false;
    }

    /**
     * Метод для получения ошибок отданых api
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function getStatusesList()
    {
        return [];
    }

}
